#!/bin/bash

set -e

source abin/ensure-deploy-vars.sh

# only run teardown for dev environments.
# we're already checking this at the teardown.sh level, but keeping it here as well as an extra precaution
# in case the script is run manually, or copied somewhere else that doesn't use the other script.
if [[ "$ENVIRONMENT" != "dev" ]];
then
    echo "Teardown only supported for stages in the dev environment." >&2
    exit 1
fi

echo "Deleting ${APP_NAME} cloudformation stack ${STACK_NAME} in ${AWS_DEFAULT_REGION}..."
aws cloudformation delete-stack --stack-name "${STACK_NAME}"
aws cloudformation wait stack-delete-complete --stack-name "${STACK_NAME}"
