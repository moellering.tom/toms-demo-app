#! /bin/bash

report_folder="./reports/"
mkdir -p "$report_folder"
dotnet_format_exit_code=0

echo ""
echo "---------------------------------------------------------------------"
echo "Linting dotnet code..."
echo ""
if [[ "$ATTEMPT_DOTNET_LINT_FIXES" == true ]]
then
    echo "DotNet Linting Config: linting WILL attempt to fix issues..."
    echo ""
    dotnet format --report "$report_folder" DEMO.API.sln # will attempt to fix whitespace, code style, and analyzer issues
    dotnet_format_exit_code=$?
else
    echo "DotNet Linting Config: attempting auto repair of linting issues skipped"
    echo ""
fi

echo ""
echo "---------------------------------------------------------------------"
echo "Linting bash scripts using shellcheck..."
echo ""

script_dir="./abin/"
shell_check_log_file="shell_check_results.txt"
shellcheck_exit_code=0
disabled_checks="SC1117"

if ! command -v shellcheck &> /dev/null
then
    echo "######################################################";
    echo "[WARN] Shellcheck is not present on this system...";
    echo ""
    echo "To install Shellcheck:"
    echo "  [debian]: sudo apt install shellcheck"
    echo "  [macos]: brew install shellcheck"
    echo "  [win]: choco install shellcheck"
else
    echo "[INFO] Shellcheck is now in progress...";
    set -o pipefail
    if [[ -z "$1" ]]
    then
        echo "Evaluating all scripts in directory: ./abin/"
        echo ""
        shellcheck --shell=bash -x -e "$disabled_checks" ${script_dir}*.sh 2>&1 | tee "$report_folder$shell_check_log_file"
        shellcheck_exit_code=$?
    else
        echo "Evaluating named script file: $1"
        echo ""
        shellcheck --shell=bash -x -e "$disabled_checks" "$1" 2>&1 | tee "$report_folder$shell_check_log_file"
        shellcheck_exit_code=$?
    fi
fi

if [[ "$dotnet_format_exit_code" != "0" ]]
then
    echo "";
    echo "######################################################";
    echo "[ERROR] Dotnet linting issues discovered!";
    echo "Running 'make lint' MAY fix the issue(s)";
    echo "Linting results logged here: $report_folder"
    exit $dotnet_format_exit_code;
else
    echo "";
    echo "######################################################";
    echo "[INFO] NO Dotnet linting issues found";
    echo "";
fi;

if [[ "$shellcheck_exit_code" != "0" ]]
then
    echo "";
    echo "######################################################";
    echo "[ERROR] Shellcheck linting issues discovered!";
    echo "Linting results logged here: $report_folder"
    exit $shellcheck_exit_code;
else
    echo "";
    echo "######################################################";
    echo "[INFO] NO shellcheck linting issues found";
    echo "";
fi;
