#!/bin/bash

set -e

source abin/ensure-deploy-vars.sh

# Authenticate to the cluster
aws eks update-kubeconfig --name "${CLUSTER}"

echo "Deleting ${APP_NAME} helm chart release ${RELEASE} on ${NAMESPACE}..."

helm uninstall --namespace="${NAMESPACE}" "${RELEASE}"
