#! /bin/bash

set -eo pipefail

# The STACK_NAME and TABLE_NAME variables are set here
source abin/ensure-deploy-vars.sh

# Retreive an ssm parameter value by name
function getParamValue() {
  aws ssm get-parameter \
    --name "${1}" \
    --query Parameter.Value \
    --output text
}

# Determine the openid connect provider.
# Hacky but right from the docs: https://docs.aws.amazon.com/eks/latest/userguide/create-service-account-iam-policy-and-role.html
OIDC_PROVIDER=$(aws eks describe-cluster --name "${CLUSTER}" --query "cluster.identity.oidc.issuer" --output text | sed -e "s/^https:\/\///")

echo "INFO: OIDC_PROVIDER: ${OIDC_PROVIDER}"

templateFile=cfn/templates/app-resources.yaml

# Because we have to use the OIDC_PROVIDER as part of a property name in the cloudformation template, we can't substitute
# it with standard cloudformation methods, so we have to pre-process it with sed and a placeholder.
# The placeholder uses double-underscores around the name instead of something more akin to our standard of curly braces
# because the underscores allow us to validate the template before it's been pre-processed (at the time of the deployer image build).
sed "s|__OIDC_PROVIDER__|${OIDC_PROVIDER}|g" < cfn/templates/app-resources-raw.yaml > "${templateFile}"

# this name should match the metadata.name property of the service account kubernetes resource
serviceAccountName=${RELEASE}

# Show Variable values for manual inspection
echo "STACK_NAME: ${STACK_NAME}"
echo "ENVIRONMENT: ${ENVIRONMENT}"
echo "STAGE: ${STAGE}"
echo "CONTEXT: ${CONTEXT}"
echo "AWS_DEFAULT_REGION: ${AWS_DEFAULT_REGION}"
echo "CLUSTER: ${CLUSTER}"
echo "serviceAccountName: ${serviceAccountName}"
echo "GIT_REPO: ${GIT_REPO}"
echo "GIT_COMMIT_HASH: ${GIT_COMMIT_HASH}"
echo "DEPLOYED_BY: ${DEPLOYED_BY}"
echo "DEPLOYMENT_JOB: ${DEPLOYMENT_JOB}"
echo "PRODUCT: ${PRODUCT}"
echo "SUBSYSTEM: ${SUBSYSTEM}"
echo "COMPONENT: ${COMPONENT}"

set -x

# Deploy!
aws cloudformation deploy \
  --template-file "${templateFile}" \
  --stack-name "${STACK_NAME}" \
  --capabilities "CAPABILITY_IAM" \
  --parameter-overrides \
      "EnvironmentName=${ENVIRONMENT}" \
      "Namespace=${NAMESPACE}" \
      "ServiceAccountName=${serviceAccountName}" \
      "Context=${CONTEXT}" \
  --tags \
      "EnvironmentName=${ENVIRONMENT}" \
      "StageName=${STAGE}" \
      "GitRepo=${GIT_REPO}" \
      "GitCommitHash=${GIT_COMMIT_HASH}" \
      "DeployedBy=${DEPLOYED_BY}" \
      "DeploymentJob=${DEPLOYMENT_JOB}" \
      "Product=${PRODUCT}" \
      "Subsystem=${SUBSYSTEM}" \
      "Component=${COMPONENT}" \
  --no-fail-on-empty-changeset

set +x
