#! /bin/bash

# use the linting script to both check AND attempt to fix the linking issues:
ATTEMPT_DOTNET_LINT_FIXES=true
source abin/check-linting.sh
