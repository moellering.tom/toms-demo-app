#! /bin/bash

set -e

tmpDir=$1

# Coverage files are dumped into randomly named (guid) folders under our 'tmpDir', we need to perform some "discovery"
# to track down the files we want to process:
echo "--------------------------------------"
echo "Generating coverage reports..."
mkdir -p 'reports/'
echo ""
echo "temp dir = ${tmpDir}"
echo ""
coverage_reports=''
count_reports=0;

for d in "${tmpDir}"/*; do

    echo "Evaluating directory: $d"
    if [ -f "$d/coverage.cobertura.xml" ]
    then
        echo "Found coverage report: $d/coverage.cobertura.xml"
        count_reports=$((count_reports+1))
        report_copy="reports/${count_reports}_coverage.cobertura.xml"
        echo "Copying coverage file to: $report_copy"
        cp "$d/coverage.cobertura.xml" "$report_copy"
        echo ""

        if [ "$coverage_reports" == '' ]
        then
            coverage_reports="$report_copy"
        else
            coverage_reports="$coverage_reports;$report_copy"
        fi
    fi

done

echo "Reports found: $coverage_reports"
echo ""
~/.dotnet/tools/reportgenerator "-reports:$coverage_reports" "-targetdir:reports/coveragereport" "-reporttypes:Html;TextSummary"

cat reports/coveragereport/Summary.txt
