#! /bin/bash

set -eo pipefail

if [ "$1" = "local" ]; then
  export CONTEXT="local"
else
  export CONTEXT="cloud"
fi

# only run teardown for dev environments.
if [[ "$ENVIRONMENT" != "dev" ]]; then
  echo "Teardown only supported for stages in the dev environment." >&2
  exit 1
fi

if [ "$CONTEXT" = "local" ]; then
  echo ===============================
  echo Local Deployment. Skipping Helm Chart
  echo ===============================
else
  echo ===============================
  echo Tearing Down Helm Chart
  echo ===============================

  ./abin/teardown-chart.sh
fi

echo ===================================
echo  Tearing Down Cloudformation Stack
echo ===================================

./abin/teardown-cfn.sh
