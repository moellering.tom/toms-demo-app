#!/bin/bash

#
# Fix env paths
#

# When using `bash` for local dev, team members are commonly using either: WSL2 or GitBash.
# While functionally these are both instances of `bash`, they handle file paths differently.
#
#   WSL2 mounts the 'c:' directory under '/mnt/c/', however WSL also has a linux root file system
#   which you can access under '/'.
#
#       Example - both of these work - but are *NOT* the same location:
#       ls -l /mnt/c/app/certs          # <------- Windows file system
#       ls -l /app/certs                # <------- Linux file system
#
#   GitBash attempts to overlay the windows file system into linux to make some familiar commands
#   for windows shell users also work in linux. This convenience however means that paths don't
#   work in the same way that they do for a true linux environment. GitBash mounts the windows
#   `c:` file system mount point as `/c/`. However you can also access the same using `c:/`. The
#   key place this matters is when you run an application from GitBash and that application attempts
#   to use the Windows Filesystem.
#
#       Example - this works from bash in GitBash:
#       ls -l /c/app/certs
#
#       However if you run a dotnet app FROM GitBash which attempts to use this path, the path will
#       not work. The following path will work in both 'bash' from GitBash as well as dotnet running
#       in GitBash:
#       ls -l c:/app/certs
#
#   This script attempts to detect which environment your using for local development and
#   sets path overrides accordingly.

echo "[INFO] Evaluating OS environment..."
echo "[INFO] Current OS environment is: $OSTYPE"

function set_bash_on_windows_paths() {
    export LOCAL_CERT_PATH='c:/app/certs'
    export Kestrel__Endpoints__Https__Certificate__Path='c:/app/certs/server.pfx'

    echo "[INFO] EnvVar LOCAL_CERT_PATH set: $LOCAL_CERT_PATH"
    echo "[INFO] EnvVar Kestrel__Endpoints__Https__Certificate__Path set: $Kestrel__Endpoints__Https__Certificate__Path"
}

# check to make sure env vars aren't already set
if [[ -n "$LOCAL_CERT_PATH" && -n "$Kestrel__Endpoints__Https__Certificate__Path" ]]; then
    echo "[INFO] EnvVar LOCAL_CERT_PATH is already set with a value of: $LOCAL_CERT_PATH"
    echo "[INFO] EnvVar Kestrel__Endpoints__Https__Certificate__Path is already set with a value of: $Kestrel__Endpoints__Https__Certificate__Path"

    # nothing to do
else
    if [[ "$OSTYPE" == *"msys"* ]]; then
        echo "[INFO] OS has been detected as a derivative of 'msys' (likely GitBash)"
        echo "[INFO] Setting paths to support the windows file system"
        set_bash_on_windows_paths

    elif [[ "$OSTYPE" == *"cygwin"* ]]; then
        echo "[INFO] OS has been detected as a derivative of 'cygwin' (bash on windows)"
        echo "[INFO] Setting paths to support the windows file system"
        set_bash_on_windows_paths

    else
        echo "[INFO] OS appears to be a linux like environment, setting defaults..."
        export LOCAL_CERT_PATH='/app/certs'
        export Kestrel__Endpoints__Https__Certificate__Path='/app/certs/server.pfx'

    fi
fi
