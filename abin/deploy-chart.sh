#! /bin/bash

set -eo pipefail

source abin/ensure-deploy-vars.sh

if [[ -z ${APP_VERSION} ]]; then
  echo "ERROR: APP_VERSION environment variable not set. Set APP_VERSION to a valid docker image tag."
  exit 1
fi

# For primary and/or integration releases we set the group order to a
# static value which is artificially high. So that it comes after (in path resolution)
# any services we split out. (which by default get very low group order numbers)
# As we split out specific services (e.g. /simulate) from this project we need those services
# to share the same-DNSed URLS which are effectively overrides on the resource paths
# exposed by enterprise-gateway WITHOUT requiring this base project to explicitly list all of its
# remaining ingress spec.rules.http.paths (e.g. /catalog, /locations, etc.)
#
# It doesn't matter for feature/local branch deploys since its extremely unlikely to have the same person
# or feature-card working on multiple projects in this space at same time.
export INGRESS_GROUP_ORDER=""
if [[ "${STAGE}" == "primary" || "${STAGE}" == "integration" ]]; then
    export INGRESS_GROUP_ORDER="850"
fi

# Authenticate to EKS
aws eks update-kubeconfig --name "${CLUSTER}"

# Retreive an ssm parameter value by name
function getParamValue() {
  aws ssm get-parameter \
    --name "${1}" \
    --query Parameter.Value \
    --output text
}

#
# Set Web Hosting Variables
#

albGroup="public-api"
# don't include environment in domain name for prod for public sites
if [[ ${ENVIRONMENT} == "prod" ]]; then
  domainName=baddarek.net
else
  domainName=${ENVIRONMENT}.baddarek.net
fi

# To use the subdomain without a suffix, set the SUBDOMAIN_STYLE environment variable to "naked"
subdomain=api
if [[ ${SUBDOMAIN_STYLE} == "naked" ]]; then
  host=${subdomain}.${domainName}
else
  host=${subdomain}-${STAGE}.${domainName}
fi

printf "Getting role arn for the deployment's IAM role..."
roleArn=$(aws cloudformation describe-stacks --stack-name "${STACK_NAME}" | jq -r '.Stacks[0].Outputs[] | select(.OutputKey=="RoleArn").OutputValue')
echo "$roleArn"

# Show Variable values for manual inspection
echo "RELEASE: ${RELEASE}"
echo "ENVIRONMENT: ${ENVIRONMENT}"
echo "STAGE: ${STAGE}"
echo "AWS_DEFAULT_REGION: ${AWS_DEFAULT_REGION}"
echo "IMAGE: ${IMAGE}"
echo "INIT_CONTAINER_IMAGE: ${INIT_CONTAINER_IMAGE}"
echo "INIT_CONTAINER_IMAGE_TAG: ${INIT_CONTAINER_IMAGE_TAG}"
echo "APP_VERSION: ${APP_VERSION}"
echo "NAMESPACE: ${NAMESPACE}"
echo "host: ${host}"
echo "albGroup: ${albGroup}"
echo "ingressGroupOrder: ${INGRESS_GROUP_ORDER}"
echo "roleArn: ${roleArn}"
echo "GIT_REPO: ${GIT_REPO}"
echo "GIT_COMMIT_HASH: ${GIT_COMMIT_HASH}"
echo "DEPLOYED_BY: ${DEPLOYED_BY}"
echo "DEPLOYMENT_JOB: ${DEPLOYMENT_JOB}"
echo "PRODUCT: ${PRODUCT}"
echo "SUBSYSTEM: ${SUBSYSTEM}"
echo "COMPONENT: ${COMPONENT}"
echo "CLUSTER: ${CLUSTER}"

set -x

# Deploy!
helm upgrade "${RELEASE}" dist/chart/"${APP_NAME}"-*.tgz \
  --install \
  --timeout 6m \
  --namespace "${NAMESPACE}" \
  --values "chart/config/values-${ENVIRONMENT}.yaml" \
  --set docker.image="${IMAGE}" \
  --set docker.tag="${APP_VERSION}" \
  --set docker.initContainerImage="${INIT_CONTAINER_IMAGE}" \
  --set docker.initContainerTag="${INIT_CONTAINER_IMAGE_TAG}" \
  --set meta.roleArn="${roleArn}" \
  --set meta.environment="${ENVIRONMENT}" \
  --set meta.stage="${STAGE}" \
  --set meta.repo="${GIT_REPO}" \
  --set meta.commit="${GIT_COMMIT_HASH}" \
  --set meta.deployedBy="${DEPLOYED_BY}" \
  --set meta.deploymentJob="${DEPLOYMENT_JOB}" \
  --set meta.product="${PRODUCT}" \
  --set meta.subsystem="${SUBSYSTEM}" \
  --set meta.component="${COMPONENT}" \
  --set infra.host="${host}" \
  --set infra.albGroup="alb-${albGroup}" \
  --set infra.ingressGroupOrder="${INGRESS_GROUP_ORDER}" \
  --wait
