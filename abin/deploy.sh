#! /bin/bash

set -eo pipefail

if [ "$1" = "local" ]; then
  export CONTEXT="local"
else
  export CONTEXT="cloud"
fi

echo ================================
echo Deploying Cloudformation Stack
echo ================================

./abin/deploy-cfn.sh

if [ "${CONTEXT}" = "local" ]; then
  echo ================================
  echo Local Deployment. Skipping Helm Chart
  echo ================================

  exit
fi

echo ================================
echo Deploying Helm Chart
echo ================================

./abin/deploy-chart.sh
