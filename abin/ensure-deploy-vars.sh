#! /bin/bash

# the variables managed in this script are required for deploy AND teardown
# shellcheck disable=SC2034

#
# AWS Authentication
#
if [[ -z ${AWS_ACCESS_KEY_ID} ]]; then
  echo "ERROR: AWS_ACCESS_KEY_ID environment variable not set."
  exit 1
fi

if [[ -z ${AWS_SECRET_ACCESS_KEY} ]]; then
  echo "ERROR: AWS_SECRET_ACCESS_KEY environment variable not set."
  exit 1
fi

if [[ -z ${AWS_DEFAULT_REGION} ]]; then
  echo "ERROR: AWS_DEFAULT_REGION environment variable not set. Set to a valid aws region [us-east-1, us-west-2]."
  exit 1
fi

#
# Deployment Context
#
if [[ -z ${APP_NAME} ]]; then
  echo "ERROR: APP_NAME environment variable not set. Set APP_NAME to the docker image base name (without the host prefix)."
  exit 1
fi

if [[ -z ${ENVIRONMENT} ]]; then
  echo "ERROR: ENVIRONMENT environment variable not set. Set to one of [dev, qa, prod] to match the correct AWS VPC."
  exit 1
fi

if [[ -z ${STAGE} ]]; then
  echo "ERROR: STAGE environment variable not set. Set to a value that disambiguates this deployment within an ENVIRONMENT."
  exit 1
fi

# make sure STAGE is all lowercase
STAGE=$(echo "${STAGE}" | tr '[:upper:]' '[:lower:]')
export STAGE

# Specific to chart deploy and teardown
NAMESPACE="user-apps-${ENVIRONMENT}"
RELEASE="${APP_NAME}-${STAGE}"

# # Specific to cfn deploy and teardown
STACK_NAME="${ENVIRONMENT}-${STAGE}-${APP_NAME}"

# Set metadata defaults for when deploying locally using bash parameter expansion
GIT_REPO=${GIT_REPO:-local}
GIT_COMMIT_HASH=${GIT_COMMIT_HASH:-local}
DEPLOYED_BY=${DEPLOYED_BY:-local}
DEPLOYMENT_JOB=${DEPLOYMENT_JOB:-local}

# Set the metadata values used for tagging (cfn) and annotations (chart) on deploy
PRODUCT=encourage
SUBSYSTEM=encourage-api
COMPONENT=api
