#! /bin/bash

#
# Handle input args:
#
docker_image=''
gitlab_docker_repo_id=''
cert_path=''

function show_usage() {
    echo ""
    echo "usage: setup-localdev-certs -c {opt: cert file path} -d {opt: docker image} -r {opt: gitlab repository id}"
    printf "\n  example: setup-localdev-certs.sh"
    printf "\n  example: setup-localdev-certs.sh -c \"/app/certs\""
    printf "\n  example: setup-localdev-certs.sh -d \"registry.gitlab.com/pickupnow/docker/pod-init-utilities:0097d152\""
    printf "\n  example: setup-localdev-certs.sh -r 1911357"
    printf "\n\n  get help: setup-localdev-certs.sh -help\n"
    echo ""
}

function eval_parameters() {
    if [ -z "$cert_path" ]
    then
        cert_path='/app/certs'
    fi

    if [ -z "$gitlab_docker_repo_id" ]
    then
        gitlab_docker_repo_id='1911357'
    fi

    show_parameters
}

function show_parameters() {
    echo ""
    echo "[setup-localdev-certs] is using parameters:"
    echo "  Cert Path: $cert_path"
    echo "  Docker Image: $docker_image"
    echo "  GitLab Docker Repository Id: $gitlab_docker_repo_id"
    echo ""
}

#
# Utility functions
#
function test_docker_exists() {
    if [ -x "$(command -v docker)" ];
    then
        echo "[INFO] Docker was found on the local system"
        return 0;
    fi

    echo "[ERROR] Docker is not present on the local system"
    exit 255
}

function test_for_docker_repo_access() {
    # There isn't really a check you can perform with the `docker` cli which will tell you if
    # you happen to be logged into a repository or not. Typically you don't hit the auth-error
    # until you try to pull or push an image.
    #
    # The following test makes a request for a non-existant image in our docker repo. We
    # **EXPECT** to get a `not found` response, any other kind of error is likely either an
    # auth error or some form of networking failure.

    docker_response=$( { docker pull registry.gitlab.com/pickupnow/docker/pod-init-utilities:0; } 2>&1 )

    if [[ "$docker_response" == *"not found"* && "$docker_response" == *"manifest unknown"* ]]
    then
        echo "[INFO] Successfully contacted our private docker repository"
    else
        echo "[ERROR] Failed contacting our private docker repository, the likely causes are:"
        echo "  - no network connection"
        echo "  - you're not currently logged into the private docker repo"
        echo ""
        echo "The error returned from docker was: $docker_response"
        echo ""
        echo "To log into the private docker repo, please use a command like the following: "
        echo ""
        echo "  docker login -u YOUR.USERNAME@pickupnow.com -p \${GITLAB_TOKEN} registry.gitlab.com"
        echo ""
        echo "Note: The '\${GITLAB_TOKEN}' env var must be pre-defined before running the above command."
        echo "If you're not sure how to do this please consult the developers handbook..."
        exit 255
    fi
}

#
# Setup certs
#
function get_docker_image() {
    if [ -z "$docker_image" ]
    then
        docker_image=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/registry/repositories/$gitlab_docker_repo_id?tags=true&tags_count=true" | jq -r '.tags[0].location')
    fi

    echo "[INFO] Detected docker image from GitLab as: $docker_image"
}

function pull_docker_image() {
    echo "[INFO] Pulling docker image, if needed: $docker_image"
    docker pull "$docker_image"
}

function create_certs() {
    echo "[INFO] Creating cert directrory if necessary: $cert_path"
    mkdir -p "$cert_path" # creates only if missing
    docker run -v "$cert_path:/app/certs" --rm "$docker_image" bash -c  "generate_cert -e dev; chmod 644 /app/certs/*"

    # If you're using WSL **AND** you want to use Visual Studio (or Rider) then we need to also copy the pfx cert
    # into the windows file system:
    if [ -d "/mnt/c" ]
    then
        echo "[INFO] Environment appears to be Windows Subsystem for Linux, copying certs into Windows..."
        mkdir -p "/mnt/c$cert_path/"
        cp "$cert_path/server.crt" "/mnt/c$cert_path/"
        cp "$cert_path/server.key" "/mnt/c$cert_path/"
        cp "$cert_path/server.pfx" "/mnt/c$cert_path/"
    fi
}

function test_certs_exist() {
    if [ ! -f "$cert_path/server.pfx" ]
    then
        exit 255
    fi
}

#
# Do stuff / 'main'
#

# handle inputs
while getopts c:d:r:h: flag
do
    case "${flag}" in
        c) cert_path=${OPTARG};;
        d) docker_image=${OPTARG};;
        r) gitlab_docker_repo_id=${OPTARG};;
        h) show_usage;;
        *) show_usage;;
    esac
done

eval_parameters

# create certs (if needed)
echo "[INFO] Setting up dev certificates at path: '$cert_path'"
echo ""

if [ ! -f "$cert_path/server.pfx" ]
then
    # check for dependencies
    test_docker_exists
    test_for_docker_repo_access

    # use docker image; create certs
    get_docker_image
    pull_docker_image
    create_certs
    test_certs_exist
else
    echo "[INFO] The certificates already exist at path '$cert_path', nothing to do..."
    echo ""
fi
