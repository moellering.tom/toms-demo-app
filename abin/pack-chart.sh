#! /bin/bash

set -e

if [[ -z ${APP_VERSION} ]]; then
  echo "ERROR: APP_VERSION environment variable not set. Set APP_VERSION to a valid docker image tag."
  exit 1
fi

helm lint chart

helm package --app-version "${APP_VERSION}" --destination dist/chart chart
