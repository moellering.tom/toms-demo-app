#! /bin/bash

set -e

# env vars needed by .net tests
export NAMESPACE=user-apps-${ENVIRONMENT}

tmpDir=$(mktemp -d)

# See https://docs.gitlab.com/ee/ci/unit_test_reports.html#net-example
dotnet test \
  --no-build \
  --configuration Release \
  --test-adapter-path:. --logger:"junit;LogFilePath=../../reports/{assembly}-test-result.xml;MethodFormat=Class;FailureBodyFormat=Verbose" \
  --filter:"DisplayName~IntegrationTests" \
  --results-directory "${tmpDir}"

rm -rf "${tmpDir}"
