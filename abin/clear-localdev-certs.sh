#!/bin/bash

echo "[INFO] Evaluating OS environment..."
echo "[INFO] Current OS environment is: $OSTYPE"

function clear_certs_from_windows() {
    win_path='c:/app/certs'
    echo "[INFO] Clearing certs using windows paths $win_path ..."
    rm -f -r "$win_path"
}

function try_clear_certs_from_wsl() {
    # For WSL we copy the certs from the linux file system into windows so that debugging in Visual Studio or Rider works as expected.
    # To cleanup the certs we need to make sure we also remove them from windows:
    if [ -d "/mnt/c" ]
    then
        wsl_path="/mnt/c$1/"
        echo "[INFO] Environment appears to be WSL, attempting to clear certs from windows mounted folder $wsl_path ..."
        rm -r -f "$wsl_path"

    fi
}

if [[ -n "$LOCAL_CERT_PATH" ]]; then
    echo "[INFO] Cert path provided using env var LOCAL_CERT_PATH, using this path: $LOCAL_CERT_PATH ..."
    rm -f -r "$LOCAL_CERT_PATH"
    try_clear_certs_from_wsl "$LOCAL_CERT_PATH"

else
    if [[ "$OSTYPE" == *"msys"* ]]; then
        echo "[INFO] OS has been detected as a derivative of 'msys' (likely GitBash)"
        clear_certs_from_windows

    elif [[ "$OSTYPE" == *"cygwin"* ]]; then
        echo "[INFO] OS has been detected as a derivative of 'cygwin' (bash on windows)"
        clear_certs_from_windows

    else
        echo "[INFO] OS appears to be a linux like environment, using default paths..."
        lin_path='/app/certs'
        rm -f -r "$lin_path"
        try_clear_certs_from_wsl "$lin_path"

    fi
fi
