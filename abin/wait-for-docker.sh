#!/bin/sh

# ^^^^^ Was having issues getting `bash` to work in the gitlab runner - not sure why,
# so had to make this script compliant with 'sh'

# Problem this script is _trying_ to resolve:
#
#   While building any of our docker images - we are randomly hitting:
#   - unable to resolve docker endpoint: open /certs/client/ca.pem: no such file or directory
#
# The following is not a *fix* but more of a _"hack"_. Online searches didn't lead me to any great fixes for this problem.... ¯\_(ツ)_/¯

#
# Do stuff / 'main'
#
echo "[INFO] Waiting for docker to be available (workaround to CI/CD timing bug)" # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27384

maxTests=15

for i in $(seq 1 $maxTests)
do
    if docker info > /dev/null 2>&1;
    then
        echo "[INFO] Docker is available, happy building..."
        break
    else
        echo "[WARN] (test# $i/$maxTests) Docker is not available - will test again..."
    fi

    sleep 1s
done

for i in $(seq 1 $maxTests)
do
    if [ -f /certs/client/ca.pem ];
    then
        echo "[INFO] Certs for docker are available, have fun..."
        exit 0
    else
        echo "[WARN] (test# $i/$maxTests) Certs for docker are not available - will test again..."
    fi

    sleep 1s
done
