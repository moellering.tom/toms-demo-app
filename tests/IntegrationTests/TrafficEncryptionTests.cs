using System.Net;
using System.Security.Authentication;
using Xunit;
using Xunit.Abstractions;

namespace IntegrationTests;

public class TrafficEncryptionTests
{
    private readonly ITestOutputHelper _testOutputHelper;
    private readonly string _ingressUrl;

    private readonly string _serviceNamespacedHostName;
    private readonly string _serviceNamespacedBaseUrl;

    public TrafficEncryptionTests(ITestOutputHelper testOutputHelper)
    {
        const string EnvVarNamespaceName = "NAMESPACE";
        const string EnvVarServiceHostnameName = "INTERNAL_SERVICE_HOSTNAME";
        const string EnvVarIngressUrlName = "INTERNAL_INGRESS_URL";

        _testOutputHelper = testOutputHelper ?? throw new ArgumentNullException(nameof(testOutputHelper));

        string k8SNamespace = Environment.GetEnvironmentVariable(EnvVarNamespaceName)
            ?? throw new ApplicationException($"Environmental Var '{EnvVarNamespaceName}' is missing");

        string serviceHostName = Environment.GetEnvironmentVariable(EnvVarServiceHostnameName)
            ?? throw new ApplicationException($"Environmental Var '{EnvVarServiceHostnameName}' is missing");

        _ingressUrl = Environment.GetEnvironmentVariable(EnvVarIngressUrlName)
            ?? throw new ApplicationException($"Environmental Var '{EnvVarIngressUrlName}' is missing");

        _serviceNamespacedHostName = $"{serviceHostName}.{k8SNamespace}";
        _serviceNamespacedBaseUrl = $"https://{_serviceNamespacedHostName}/";
    }

    [Fact]
    public async void InternalToK8SHttpRequestTimesOut()
    {
        // Test's purpose is to verify that we only support HTTPS internal in k8s, any HTTP-->HTTPS redirects will
        // happen at the ALB layer. Behind the ALB we only support HTTPS.
        var insecureUrl = $"http://{_serviceNamespacedHostName}/";
        _testOutputHelper.WriteLine($"Testing url: {insecureUrl}");

        var httpClient = new HttpClient() {Timeout = TimeSpan.FromSeconds(10)};

        await Assert.ThrowsAsync<TimeoutException>(async () =>
        {
            try
            {
                await httpClient.GetAsync($"{insecureUrl}");
            }
            catch (TaskCanceledException ex)
            {
                throw ex.InnerException;
            }
        });
    }

    [Fact]
    public async void HttpsRequestIsExpectedTlsVersion()
    {
        _testOutputHelper.WriteLine($"Testing url: {_serviceNamespacedBaseUrl}");

        var handler = GetSecureHandler();

        handler.ServerCertificateCustomValidationCallback += (_, certificate, _, _) =>
        {
            _testOutputHelper.WriteLine(certificate?.Subject);
            return true;
        };

        var httpClient = new HttpClient(handler);

        // will throw if we can't get an SSL connection with TLS1.2 or TLS1.3
        var result = await httpClient.GetAsync($"{_serviceNamespacedBaseUrl}");

        Assert.NotNull(result);
    }

    [Fact]
    public async void SslCertIsFromExpectedOrigin()
    {
        // Accommodating sites with landing page redirects means using the healthcheck endpoint
        var url = $"{_serviceNamespacedBaseUrl}health";
        _testOutputHelper.WriteLine($"Testing url: {url}");

        // we only want to support TLS1.2 or TLS1.3
        var handler = GetSecureHandler();

        handler.ServerCertificateCustomValidationCallback += (_, certificate, _, _) =>
        {
            _testOutputHelper.WriteLine(certificate?.Subject);
            Assert.NotNull(certificate?.Subject);
            Assert.Contains("pickupnow.com", certificate.Subject);
            return true;
        };

        var httpClient = new HttpClient(handler);

        var result = await httpClient.GetAsync($"{url}");

        Assert.NotNull(result);
    }

    [Fact]
    public async void IngressHealthCheckReturnsOk()
    {
        var url = $"{_ingressUrl}/status";
        _testOutputHelper.WriteLine($"Testing url: {url}");

        // we only want to support TLS1.2 or TLS1.3
        var httpClient = new HttpClient(GetSecureHandler());

        var result = await httpClient.GetAsync($"{url}");

        Assert.NotNull(result);
        Assert.Equal(HttpStatusCode.OK, result.StatusCode);
    }

    private HttpClientHandler GetSecureHandler()
    {
        return new HttpClientHandler() { SslProtocols = SslProtocols.Tls13 | SslProtocols.Tls12 };
    }
}
