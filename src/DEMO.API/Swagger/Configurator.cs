using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.SwaggerGen;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace DEMO.API.Swagger
{
    /// <summary>
    /// Helper class to clean swagger and swagger ui configuration out of the startup class
    /// </summary>
    public static class Configurator
    {
        public static void RegisterSwagger(IServiceCollection services)
        {
            // the versioning and swagger code came mostly from https://github.com/microsoft/aspnet-api-versioning/tree/master/samples/aspnetcore/SwaggerSample
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
            services.AddSwaggerGen(options =>
            {
                // add a custom operation filter which set default values
                options.OperationFilter<SwaggerDefaultValues>();
                options.EnableAnnotations();
                options.CustomSchemaIds(ConfigureSchemaNames);
                // order the actions--this sort order isn't exactly what I want but don't wanna lose the code
                options.OrderActionsBy(apiDesc =>
                    $"{apiDesc.GroupName}_{apiDesc.RelativePath}_{apiDesc.HttpMethod}");

                // integrate xml comments
                options.IncludeXmlComments(XmlCommentsFilePath);
            });

            // must come after AddSwaggerGen (remove if we convert to System.Text.Json)
            services.AddSwaggerGenNewtonsoftSupport();
        }

        private static string ConfigureSchemaNames(Type type)
        {
            if (type.FullName == "Microsoft.AspNetCore.Mvc.ProblemDetails")
                return "ProblemDetails";

            // simplify the schema names based on the namespace of the models classes
            return type.FullName
                .Replace("DEMO.API.", string.Empty)
                .Replace(".Models", string.Empty)
                .Replace("Pricing.Pricing.", "Pricing.")
                .Replace("Scheduling.Scheduling.", "Scheduling.");
        }

        public static void ConfigureSwaggerUI(SwaggerUIOptions options, IApiVersionDescriptionProvider versionProvider)
        {
            // don't allow "Try it now"
            options.SupportedSubmitMethods(submitMethods: Array.Empty<SubmitMethod>());

            options.InjectStylesheet("/swagger-pickup.css");

            foreach (var description in versionProvider.ApiVersionDescriptions.Where(d => !d.IsDeprecated))
            {
                options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
            }

            options.RoutePrefix = string.Empty;
        }

        private static string XmlCommentsFilePath
        {
            get
            {
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var fileName = $"{typeof(Startup).GetTypeInfo().Assembly.GetName().Name}.xml";
                return Path.Combine(basePath, fileName);
            }
        }
    }
}
