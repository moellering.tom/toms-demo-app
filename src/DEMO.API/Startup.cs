using System.Text.Json.Serialization;
using Amazon.SimpleNotificationService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;

using DEMO.API.Encourage.Service;
using DEMO.API.Encourage.Service.HealthChecks;

namespace DEMO.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                });

            services.AddCors();

            RegisterApiVersioning(services);
            Swagger.Configurator.RegisterSwagger(services);

            RegisterHealthChecks(services);

            services.AddMemoryCache();

            RegisterApplicationServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IApiVersionDescriptionProvider versionProvider)
        {
            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHealthChecks(path: "/status");
            app.UseStaticFiles();

            // only exposed in dev until we're ready to roll out the openapi changes.
            // Once we're ready, we should expose it in all environments, including prod.
            if (env.IsDevelopment() || env.IsEnvironment("dev"))
            {
                app.UseSwagger();
                app.UseSwaggerUI(options => Swagger.Configurator.ConfigureSwaggerUI(options, versionProvider));
            }

            var list = new List<string>();
            Configuration.GetSection("CORS").Bind(list);
            app.UseCors(builder =>
                builder.AllowAnyMethod().AllowAnyHeader().WithOrigins(list.ToArray()));

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void RegisterApplicationServices(IServiceCollection services)
        {
            services.AddScoped<IEncourageService, EncourageService>();
        }

        private static void RegisterHealthChecks(IServiceCollection services)
        {
            services.AddHealthChecks().AddCheck<EncourageHealthCheck>("Encourage Service");
        }

        private static void RegisterApiVersioning(IServiceCollection services)
        {
            // the versioning and swagger code came mostly from https://github.com/microsoft/aspnet-api-versioning/tree/master/samples/aspnetcore/SwaggerSample
            services.AddApiVersioning(options =>
            {
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.AssumeDefaultVersionWhenUnspecified = false;
                // reporting api versions will return the headers "api-supported-versions" and "api-deprecated-versions"
                options.ReportApiVersions = true;
                options.ApiVersionReader = new UrlSegmentApiVersionReader();
            });
            services.AddVersionedApiExplorer(options =>
            {
                // add the versioned api explorer, which also adds IApiVersionDescriptionProvider service
                // note: the specified format code will format the version as "'v'major[.minor][-status]"
                options.GroupNameFormat = "'v'VVV";

                // note: this option is only necessary when versioning by url segment. the SubstitutionFormat
                // can also be used to control the format of the API version in route templates
                options.SubstituteApiVersionInUrl = true;
            });
        }
    }
}
