using System.Security.Authentication;
using Microsoft.AspNetCore.Server.Kestrel.Core;

namespace DEMO.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                        .UseKestrel(UseKestrelAction)
                        .UseStartup<Startup>();
                });

        private static void UseKestrelAction(WebHostBuilderContext context, KestrelServerOptions serverOptions)
        {
            serverOptions.Limits.MaxRequestBodySize = 737280000;

            if (string.IsNullOrEmpty(context.Configuration.GetSection("Kestrel:Endpoints:Https:Url").Value))
                return;

            serverOptions.Configure(context.Configuration.GetSection("Kestrel"))
                .Endpoint("HTTPS", listenOptions =>
                {
                    // force the system to only support SSL protocols we require (i.e. don't allow down-grade to TLS1.0 or TLS1.1)
                    listenOptions.HttpsOptions.SslProtocols = SslProtocols.Tls12 | SslProtocols.Tls13;
                });
        }
    }
}
