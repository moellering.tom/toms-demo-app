using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DEMO.API.Encourage.Models;
using DEMO.API.Encourage.Service;
using Swashbuckle.AspNetCore.Annotations;

namespace DEMO.API.Encourage.v1
{
    [ApiVersion("1.0")]
    [Route("v{v:apiVersion}/encourage")]
    [ApiController]
    // [Authorize]
    public class EncourageController : ControllerBase
    {
        private const string SWAGGER_TAG = "Deliverability - Estimate";

        private readonly ILogger<EncourageController> _logger;
        private readonly IEncourageService _encourageService;

        public EncourageController(
            ILogger<EncourageController> logger,
            IEncourageService encourageService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _encourageService = encourageService ?? throw new ArgumentNullException(nameof(EncourageService));
        }

        /// <summary>
        /// ???
        /// </summary>
        /// <remarks>
        /// </remarks>
        [HttpGet()]
        [Produces("application/json")]
        [ProducesResponseType(typeof(EncourageResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [SwaggerOperation(Tags = new[] { SWAGGER_TAG })]
        public IActionResult GetEncouragement()
        {
            var response = _encourageService.GetEncouragement();
            // TODO: add estimate service
            // change to timing service
            return Ok(response);
        }
    }
}
