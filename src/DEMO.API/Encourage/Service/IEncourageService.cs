﻿using System.Threading.Tasks;
using DEMO.API.Encourage.Models;

namespace DEMO.API.Encourage.Service
{
    public interface IEncourageService
    {
        EncourageResponse GetEncouragement();
    }
}
