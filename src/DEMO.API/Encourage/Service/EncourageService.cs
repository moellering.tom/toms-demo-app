﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using DEMO.API.Encourage.Models;

namespace DEMO.API.Encourage.Service
{
    public class EncourageService : IEncourageService
    {
        public EncourageService()
        {
        }

        public EncourageResponse GetEncouragement()
        {
            EncourageResponse response = new EncourageResponse
            {
                Message = "Automate all the things!",
                Timestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds().ToString()
            };

            return response;
        }
    }
}
