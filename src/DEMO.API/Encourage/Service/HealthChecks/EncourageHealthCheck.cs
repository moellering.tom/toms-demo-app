﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace DEMO.API.Encourage.Service.HealthChecks
{
    public class EncourageHealthCheck : IHealthCheck
    {
        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellation = new CancellationToken())
        {
            return await Task.FromResult(HealthCheckResult.Healthy());
        }
    }
}
