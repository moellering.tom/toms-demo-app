﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace DEMO.API.Encourage.Models
{
    public class EncourageResponse
    {
        public string Message { get; set; }
        public string Timestamp { get; set; }
    }
}




