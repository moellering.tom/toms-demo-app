.PHONY: build run \
        start curl curls \
        set-executable ensure-reports clean

export SHELL=bash
export APP_NAME=encourage-api

# These variables should be supplied by the CI tool,
# but are set to sane defaults when running locally.

USERNAME := $(shell id -un | tr -d '[:space:].' | tr [:upper:] [:lower:] | sed -e "s/azuread+//g")
export APP_VERSION?= $(USERNAME)

# Build time required variables
export IMAGE ?= registry.gitlab.com/moellering.tom/toms-demo-app
BUILD_TAG ?= build
RELEASE_TAG ?= $(APP_VERSION)
DEPLOY_TAG ?= deploy
CURRENT_DIR = $(shell pwd)
ADDITIONAL_DOCKER_PARAMS ?=

# These variables are required for deployment and teardown (default region needed at build time for cloudformation validation)
export ENVIRONMENT ?= dev
export STAGE?= $(USERNAME)

# environment variables needed for testing:
export INTERNAL_SERVICE_HOSTNAME ?= $(APP_NAME)-$(STAGE)
export INTERNAL_INGRESS_URL ?= https://api-$(STAGE).dev.baddarek.net

# The deploy and teardown jobs authenticate to eks before doing the deploy or teardown
# Make sure you have AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY set.
export AWS_DEFAULT_REGION := us-west-2

# We make use of an init container to generate certs for us - the following identifies which
# init container image to use.
export INIT_CONTAINER_IMAGE ?= registry.gitlab.com/pickupnow/docker/pod-init-utilities
export INIT_CONTAINER_IMAGE_TAG ?= 0097d152 # only use images from the *master* branch!!!

# when running locally, expose the api on this port
LOCAL_PORT = 8000
LOCAL_HTTPS_PORT = 8001

export CLUSTER := $(ENVIRONMENT)-eks-tommoellering

# ==============
# Docker rules
# ==============

# build all three images: the build and release images as well as the deployer image
# Since this rule is first, you can run it with just "make"
build-all: build build-deploy

# The build rule is designed to be run locally as well as from the ci tool.
# Builds both build and release-tagged images
# The image created during the build contains the environment and scripts to do pre and post-deployment testing.
# We first build the image, then execute the commit_test.sh script in a container based on the image,
# being sure to mount the local reports folder, so that we can capture the output of the tests,
# and expose it to our CI server.
build: set-executable ensure-reports
	# build the base stage's image, which can run test scripts
	docker build \
		--target build \
		--cache-from $(IMAGE):$(BUILD_TAG) \
		--tag $(IMAGE):$(BUILD_TAG) \
		--build-arg NUGET_REPO=$(NUGET_REPO) \
		--build-arg NUGET_REPO_USER=$(NUGET_REPO_USER) \
		--build-arg NUGET_REPO_ACCESS_TOKEN=$(NUGET_REPO_ACCESS_TOKEN) \
		.

	# run the commit test script against the image we just built
	# the "//" on the target of the bind is so that windows machines doesn't choke (cygwin).
	docker run \
		--rm \
		--mount type=bind,source=$(shell pwd)/reports,target=//app/reports \
		$(IMAGE):$(BUILD_TAG)

	# now build the release stage's image
	docker build \
		--cache-from $(IMAGE):$(BUILD_TAG) \
		--cache-from $(IMAGE):$(RELEASE_TAG) \
		--tag $(IMAGE):$(RELEASE_TAG) \
		--build-arg NUGET_REPO=$(NUGET_REPO) \
		--build-arg NUGET_REPO_USER=$(NUGET_REPO_USER) \
		--build-arg NUGET_REPO_ACCESS_TOKEN=$(NUGET_REPO_ACCESS_TOKEN) \
		.

build-deploy: set-executable
	# build the deployer image, which contains scripts, the cloudformation template and the helm chart
	docker build \
		--tag $(IMAGE):$(DEPLOY_TAG) \
		--file Dockerfile.deployer \
		--build-arg AWS_ACCESS_KEY_ID \
		--build-arg AWS_SECRET_ACCESS_KEY \
		--build-arg AWS_SESSION_TOKEN \
		--build-arg AWS_DEFAULT_REGION \
		--build-arg APP_VERSION \
		.

#build-tester:  @ Build image used for test execution (the 'tester' docker image)
build-tester: set-executable
	docker build \
		--cache-from $(IMAGE):$(TEST_CACHE_TAG) \
		--tag $(IMAGE):$(TEST_TAG) \
		--file Dockerfile.qa-tester \
		.

	# run linting within the image we just built:
	docker run $(ADDITIONAL_DOCKER_PARAMS) \
		--rm \
		$(IMAGE):$(TEST_TAG) \
		npx eslint *.js

run: setup-dev-certs
	. abin/fix-localdev-env.sh; \
	docker run \
		-it \
		--rm \
		--publish $(LOCAL_PORT):8000 \
		--publish $(LOCAL_HTTPS_PORT):8001 \
		--env AWS_ACCESS_KEY_ID \
		--env AWS_SECRET_ACCESS_KEY \
		--env AWS_SESSION_TOKEN \
		--env AWS_REGION=$$AWS_DEFAULT_REGION \
				--mount type=bind,source=$$LOCAL_CERT_PATH,target=//app/certs \
		$(IMAGE):$(RELEASE_TAG)

# needed to test the deployment locally (make build-all, make push, make deploy, make teardown)
push:
	docker push $(IMAGE):$(APP_VERSION)

# The docker run commands for deploy and teardown provide the "interface" that lays out exactly what
# environment variables are needed for each operation

deploy:
	docker run \
		--rm \
		--env AWS_ACCESS_KEY_ID \
		--env AWS_SECRET_ACCESS_KEY \
		--env AWS_SESSION_TOKEN \
		--env AWS_DEFAULT_REGION \
		--env IMAGE \
		--env INIT_CONTAINER_IMAGE \
		--env INIT_CONTAINER_IMAGE_TAG \
		--env APP_NAME \
		--env APP_VERSION \
		--env CLUSTER \
		--env ENVIRONMENT \
		--env STAGE \
		--env SUBDOMAIN_STYLE \
		--env GIT_REPO \
		--env GIT_COMMIT_HASH \
		--env DEPLOYED_BY \
		--env DEPLOYMENT_JOB \
		$(IMAGE):$(DEPLOY_TAG) \
		abin/deploy.sh

teardown:
	docker run \
		--rm \
		--env AWS_ACCESS_KEY_ID \
		--env AWS_SECRET_ACCESS_KEY \
		--env AWS_SESSION_TOKEN \
		--env AWS_DEFAULT_REGION \
		--env APP_NAME \
		--env APP_VERSION \
		--env CLUSTER \
		--env ENVIRONMENT \
		--env STAGE \
		$(IMAGE):$(DEPLOY_TAG) \
		abin/teardown.sh

integration-test:
	docker run \
		--rm \
		--env ENVIRONMENT \
		--env INTERNAL_SERVICE_HOSTNAME \
		--env INTERNAL_INGRESS_URL \
		--mount type=bind,source=$(shell pwd)/reports,target=//app/reports \
		$(IMAGE):$(BUILD_TAG) \
		abin/integration-test.sh

# ==============
# Convenience Rules for getting a bash command line in each container
# ==============

bash-build:
	docker run --rm -it $(IMAGE):$(BUILD_TAG) bash

bash-release:
	docker run --rm -it --entrypoint bash $(IMAGE):$(RELEASE_TAG)

bash-deploy:
	docker run --rm -it $(IMAGE):$(DEPLOY_TAG)

# ==============
# Local rules
# ==============

# start the app on your box (not in a container)
start: setup-dev-certs
	. abin/fix-localdev-env.sh; \
	dotnet run --project src/DEMO.API/DEMO.API.csproj

setup-dev-certs: set-executable
	. abin/fix-localdev-env.sh; \
	abin/setup-localdev-certs.sh -c "$$LOCAL_CERT_PATH"

# needs to be run as `sudo make clear-dev-certs` if using WSL, sudo not present on GitBash ¯\_(ツ)_/¯
clear-dev-certs: set-executable;
	abin/clear-localdev-certs.sh; \

coverage: set-executable clean
	dotnet build --configuration Release
	./abin/commit-test.sh

# reports problems but does not fix
check-linting: set-executable;
	abin/check-linting.sh

# attempts to automagically fix linting issues
lint: set-executable
	abin/run-linting.sh

# ==============
# Manual Testing rules
# ==============

health:
	@curl --write-out "\n%{http_code}\n" http://localhost:$(LOCAL_PORT)/status

# ==============
# Utility rules
# ==============

set-executable:
	chmod +x abin/*.sh

ensure-dirs:
	mkdir -p artifacts
	mkdir -p $(TEST_RESULTS_FOLDER)

ensure-reports:
	mkdir -p reports

# For prod releases we don't have access to the artifact containing this file, so we
# need to test that the file exists before trying to run it.
wait-for-docker: set-executable
	if [ -e abin/wait-for-docker.sh ]; then abin/wait-for-docker.sh; fi

clean:
	rm -rf reports out
