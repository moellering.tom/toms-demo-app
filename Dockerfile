FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build

WORKDIR /app

ARG NUGET_REPO
ARG NUGET_REPO_USER
ARG NUGET_REPO_ACCESS_TOKEN

RUN apt update && apt install -y curl shellcheck && apt clean && \
    dotnet tool install --global dotnet-reportgenerator-globaltool

COPY nuget.config .
COPY src/DEMO.API/DEMO.API.csproj ./src/DEMO.API/DEMO.API.csproj
COPY tests/IntegrationTests/IntegrationTests.csproj ./tests/IntegrationTests/IntegrationTests.csproj
COPY DEMO.API.sln ./

RUN dotnet restore

COPY . ./

# running build separate from publish so that when we run tests,
# then the libraries will be in the expected directories.
# If we only build as part of publish, the test dlls won't be built
# and the tests won't run.
RUN ./abin/check-linting.sh
RUN dotnet build --no-restore --configuration Release -warnAsError
RUN dotnet publish --no-build --configuration Release --output out src/DEMO.API/DEMO.API.csproj

FROM mcr.microsoft.com/dotnet/aspnet:6.0

WORKDIR /app

COPY --from=build /app/out .

# Default to Development (aka "local") environment to load settings from the appsettings.Development.json
ENV ASPNETCORE_ENVIRONMENT=Development
ENV ASPNETCORE_URLS=http://+:8000,https://+:8001
EXPOSE 8000 8001

# Setting the group id explicitly so we can use it in the deployment's pod's securityContext
# (see chart/templates/deployment.yaml)
RUN addgroup --gid 1000 dotnet && \
    adduser --gid 1000 --disabled-password --gecos '' dotnet

USER dotnet

ENTRYPOINT ["dotnet", "DEMO.API.dll"]
